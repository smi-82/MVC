<?php

require_once ROOT.'/configs/db-config.php';

/**
 * Class DBManager
 *
 * класс оберка для работы с PDO
 */
class DBManager
{
    private static $instance = null;
    private $host;
    private $pdo;

    /**
     * @param $dbname
     * создание БД
     */
    private function DBCreate($dbname)
    {
        $str='CREATE DATABASE IF NOT EXISTS '.$dbname.' COLLATE utf8_general_ci';
        $this->pdo->exec($str);
    }

    /**
     * создание таблиц
     */
    private function CreateTable()
    {

    }

    /**
     * @return mixed
     *
     * получение подключения
     */
    public function getPDO()
    {
        return $this->pdo;
    }
    public function __construct()
    {
        $dbConfig = config();
        $this->host = $dbConfig['host'];
        $this->pdo = new PDO ('mysql:host='.$this->host,$dbConfig['user'],$dbConfig['password']);
        $this->pdo->exec('SET NAMES utf8');
        $this->DBCreate($dbConfig['dbname']);
        $dbName = $dbConfig['dbname'];
        $this->pdo->query("USE $dbName");
    }

    /**
     * @return DBManager|null
     * создание подключения
     */
    static public function getInstance()
    {
        if(!self::$instance)
            self::$instance = new self();
        return self::$instance;
    }

    /**
     * @param null $table
     * @param array $data
     * @return bool
     *
     * метод простой вставки записи в БД
     */
    public function insert($table = null, array $data = array())
    {
        if(empty($data) || $table == null)
            return false;

        $query = 'INSERT INTO '.$table.' ('.implode(", ",array_keys($data)).') VALUES ('.substr(str_repeat("?, ", count($data)),0,-2).')';

        $tmp = $this->pdo->prepare($query);

        return $tmp->execute(array_values($data));
    }

    /**
     * @param null $table
     * @param array $data
     * @param array $where
     * @return bool
     *
     * метод простого обновления записей в БД
     */
    public function update($table = null, array $data = array(), array $where = array())
    {
        if(empty($data) || $table == null)
            return false;

        $tmp = array_map(function($n){return $n.'=?';}, array_keys($data));

        $arr = array_values($data);
        if(empty($where)) {
            $query = 'UPDATE ' . $table . ' SET ' . implode(', ', $tmp);
        }
        else{
            $wh = array();
            foreach($where as $key=>$val){
                if(is_array($val)){
                    $wh[] = $key.' IN ('.substr(str_repeat('?, ', count($val)),0,-2).') ';
                    $arr = array_merge($arr,array_values($val));
                }
                else{
                    $wh[] = $key.'=?';
                    $arr[] = $val;
                }
            }

            $query = 'UPDATE ' . $table . ' SET ' . implode(', ', $tmp).' WHERE '. implode(' AND ', $wh);
            unset($wh);
        }
        unset($tmp);
        return $this->pdo->prepare($query)->execute($arr);
    }

    /**
     * @param null $table
     * @param array $where
     * @return bool
     *
     * метод удаления записей
     */
    public function delete($table = null, array $where = array())
    {

        if($table == null)
            return false;
        if(empty($where)){
            $query = 'DELETE FROM '.$table;
            return $this->pdo->exec($query);
        }
        else{
            $arr = array();
            $wh = array();

            foreach($where as $key=>$val){
                if(is_array($val)){
                    $wh[] = $key.' IN ('.substr(str_repeat('?, ', count($val)),0,-2).') ';
                    $arr = array_merge($arr,array_values($val));
                }
                else{
                    $wh[] = $key.'=?';
                    $arr[] = $val;
                }
            }

            $query = 'DELETE FROM '.$table.' WHERE ('.implode(' AND ', $wh).')';
            unset($wh);
            $res = $this->pdo->prepare($query);

            return $res->execute($arr);
        }
    }
    public function lastID()
    {
        return $this->getInstance()->getPDO()->lastInsertId();
    }
}
