<?php

/**
 * Class Router
 *
 * Главный контроллер
 */
class Router
{
    private $routes;

    /**
     * конструктор получает и сохраняет массив роутов
     */
    public function  __construct()
    {
        $routesPath = ROOT.'/configs/routes.php';
        $this->routes = include($routesPath);
    }

    /**
     * @return string
     *
     * считывает строку запроса
     *
     */
    private function getURI()
    {
        if(!empty($_SERVER['REQUEST_URI']))
            return trim($_SERVER['REQUEST_URI'],'/');
        return '';
    }

    /**
     *  сравнивает строку запроса с роутами
     *  при наличии совпадений парсит роут разбирая его на модуль, имя контроллера, метод контроллера
     *  и дополнительные параметры при их наличии.
     *  Подключает файл контроллера, создает его обьект и вызывает соответствующий метод
     *
     */
    public function run()
    {
        $uri = $this->getURI();

        /**
         *  цикл по всем роутам
         */
        foreach ($this->routes as $uriPattern => $path) {

            /**
             * если строка запроса пуста, то устанавливается главная страница
             */
            if($uri == '')
                $uri = 'index';
            /**
             * сравнение строки запроса с текущим роутом
             */
            if (preg_match('~' . $uriPattern . '~', $uri)) {


                /**
                 * Подставляем параметры во внутренний роут по регулярке
                 */

                $segments = preg_replace("~$uriPattern~", $path, $uri);
                $segments = substr($segments,0,strpos($segments,'?'));
                
                /**
                 * парсит роут
                 */

                $segments = explode('/', $segments);

                /**
                 * получает имя модуля
                 */
                /**
                 * получает имя контроллера
                 */
                $controllerName = array_shift($segments) . 'Controller';

                /**
                 * получает имя метода
                 */

                $actionName = ucfirst(array_shift($segments));

                /**
                 * собирает имя файла
                 */
                $controllerFile = ROOT .'/controllers/' . $controllerName . '.php';
                /**
                 * подключает файл контроллера при его наличии
                 */
                print_r($segments);
                if (file_exists($controllerFile)) {
                    include_once $controllerFile;
                    /**
                     * создает обьект контроллера и вызывает его метод
                     */
                    $controllerObject = new $controllerName();
                    $result = call_user_func_array(array($controllerObject, $actionName),$segments);
                    if ($result !== false)
                        break;
                }
            }

        }
    }
}