<?php

/**
 * Class View
 */

abstract class View
{
    /**
     * @var string
     * свойство для хранения мени шаблона
     */
    public $layaut;
    private $vars;
    private $scripts;
    private $styles;
    /**
     * подключает файлы
     */
    private function includeFile($file)
    {
        try{
            if(file_exists($file))
                require_once $file;
            else
                throw new Exception($file.' does not exists');
        }catch (Exception $e){
            echo $e->getMessage();
            die;
        }
    }
    public function __call($name, $args)
    {
        $arr = array('getConnections','getHead','getFooter','getSidebar','getHeader');
        if(in_array($name,$arr)) {
            $args = empty($args) ? '' : '-' . array_shift($args);
            switch ($name) {
                case 'getConnections':
                    $this->includeFile(CONNECTIONS . 'connection' . $args . '.php');
                    break;
                case 'getHead':
                    $this->includeFile(TEMPLATES . 'head' . $args . '.php');
                    break;
                case 'getFooter':
                    $this->includeFile(TEMPLATES . 'footer' . $args . '.php');
                    break;
                case 'getSidebar':
                    $this->includeFile(TEMPLATES . 'sidebar' . $args . '.php');
                    break;
                case 'getHeader':
                    $this->includeFile(TEMPLATES . 'header' . $args . '.php');
                    break;
            }
        }
    }
    public function __get($name)
    {
        if(isset($this->vars[$name]))
            return $this->vars[$name];
        else{
            echo 'Variable '.$name.' does not exists';
            die;
        }
    }
    public function __set($name,$value){$this->vars[$name] = $value;}
    public function __construct()
    {
        $this->layaut = 'index';
        $this->vars = array();
        $this->scripts = array();
        $this->styles = array();
    }

    /**
     * метод отображения шаблона
     */
    protected function showLayout()
    {
        $path = ROOT.'/layouts/'.$this->layaut.'.php';
        try{
            if(file_exists($path))
                require_once $path;
            else
                throw new Exception('Layaut '.$this->layaut.' does not exists');
        }catch (Exception $e){
            echo $e->getMessage();
            die;
        }
    }
    protected function setStyles(array $styles = array())
    {
        $this->styles = array();
        if(!empty($styles)){
            $this->styles = array_merge($this->styles, $styles);
        }
    }
    protected function getStyles()
    {
        foreach($this->styles as $styles){
            echo '<link rel="stylesheet" href="'.STYLES.$styles.'">';
        }
    }
    public function setScripts(array $scripts = array())
    {
        $this->scripts = array();
        if(!empty($scripts)){
            $this->scripts = array_merge($this->scripts, $scripts);
        }
    }
    public function getScripts()
    {
        foreach($this->scripts as $script){
            echo '<script src="'.SCRIPTS.$script.'"></script>';
        }
    }
    abstract public function run();
}