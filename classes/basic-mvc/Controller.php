<?php

/**
 * Class Controller
 * подключает файлы модели и представления
 *
 */
include HELPER;
class Controller extends Helper
{
    /**
     * @param string $name
     * @param $obj
     * @return mixed
     */
    private function getObj($name = '', $obj)
    {
        if(!empty($name)){
            $file = $name.$obj.'.php';
            $file = ROOT.'/'.mb_strtolower($obj).'s/'.$file;
            try{
                if(file_exists($file)){
                    require_once $file;
                    $cl = $name.$obj;
                    return new $cl();
                }
                else
                    throw new Exception($obj.' '.$name.' does not exists');
            }catch (Exception $e){
                echo $e->getMessage();
                die;
            }
        }
    }

    /**
     * @return View
     *
     * возвращает обьект представления
     */

    public function getView($name = '')
    {
        return $this->getObj($name,'View');
    }

    /**
     * @return Model
     * возвращает обьект модели
     */
    public function getModel($name = ''){
        return $this->getObj($name,'Model');
    }

}