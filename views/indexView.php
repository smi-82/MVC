<?php

include_once VIEW;

/**
 * Class IndexView
 *
 * вид для отображения шаблона index (по умолчанию)
 */
class IndexView extends View
{
    public function run()
    {
        $this->showLayout();
    }
}