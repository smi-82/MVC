<?php
include_once CONTROLLER;

/**
 * Class PersonalAreaController
 * контроллер для обработки данных на странице index
 */
class IndexController extends Controller
{
    /**
     * вызов вида
     */
    public function index()
    {
        $view = $this->getView('index');
    }
}