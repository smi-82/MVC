jQuery(document).ready(function($){
    app.init();
});
/**
 *
 * @type {{add_list: Function, query: Function, data: Function, handlers: {thumbnail: Function, admin_check: Function, message: Function}}}
 * основные методы приложения для всех страниц сайта
 */
var app = (function(){
    return {
        init: function(){
            for(var k in this){
                var type = typeof (this[k]);
                if(type == 'object' && this[k]['init'] != undefined){
                    this[k].init();
                }
            }
        },
        /**
         * метод создания списка постов из переданного массива
         * @param obj
         * @param tpl
         * @param body
         * @param fl
         */
        add_list:function(obj ,tpl, body, fl){
            if(fl != false)
                body.empty();
            var clone;
            $.each(obj,function(ind, data){
                clone = $(tpl).clone().removeAttr('id style');
                app.data($(clone), data);
                clone.appendTo(body);
            })
        },
        /**
         * метод оберка ajax запроса
         * @param data
         * @param func
         */
        query: function(data, func){
            data = $.extend({controller: 'index'},data);
            if('data' in data)
                data.data = JSON.stringify(data.data);
            $.post('/app/functions.php', data, function(data){
                if(data) data = JSON.parse(data);
                if(func != undefined)
                    func(data);
            });
        },
        /**
         * метод добавления данных из БД путем поиска параметра data-db на DOM элементе
         * @param elem
         * @param data
         */
        data: function(elem, data, obj){
            if(obj == undefined)
                obj = this;
            for(var k in data){
                checkElem(elem);
                $.each($(elem).find('[data-db = "'+k+'"]'), function(ind, el){
                    checkElem(el);
                });
            }
            function checkElem(el){
                if($(el).attr('data-db') == k){
                    var handler = $(el).attr('data-handler');
                    if(handler != undefined){
                        obj.handlers[handler]($(el),data);
                    }
                    else{
                        if( ['INPUT','TEXTAREA'].indexOf(el.tagName) >= 0)
                            $(el).val(data[k]);
                        else{
                            if($(el).children().length)
                                $(el).prepend(data[k]);
                            else
                                $(el).html(data[k]);
                        }
                    }
                }
            }
        },

        /**
         * Объект методов для дополнительной обработки DOM элементов на основании
         * данных из БД. работает при наличи атрибута data-handler на DOM элементе
         */
        handlers:{
            func: function(elem, data){

            }
        }
    };
})();
