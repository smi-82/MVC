<?php

if(isset($_POST['func'])){
    $func = 'controller';
    echo $func();
    unset($_POST['func']);
    if(isset($_POST['data']))
        unset($_POST['data']);
}

/**
 * создает строку с классами bootstrap
 * принимает следующие параметры:
 *  - число (количество колонок сетки);
 *  - массив (со строковыми или числовыми ключами);
 *  - без параметров
 * возвращает строку по умолчанию col-xs-$size col-sm-$size col-md-$size col-lg-$size,
 * или строку с заданными параметрами.
 */
function getBsCl($size = 12){

    $str = 'col-';
    $tmp = ' ';
    $fl = false;
    if(is_array($size)){
        if(empty($size))
            $fl = true;
        else{
            foreach($size as $key=>$val){

                if(is_int($key))
                    $tmp.=$str.$val.'-12 ';
                else
                    $tmp.=$str.$key.'-'.$val.' ';
            }
            unset($size);
        }
    }
    else
        $fl = true;

    if($fl){
        $screens = array('xs','sm','md','lg');

        foreach($screens as $val)
            $tmp.=$str.$val.'-'.$size.' ';

        unset($screens);
    }
    return $tmp;
}

/**
 * функция дебага
 */
function debug($arr, $fl = true){
    $obj = Helper::getInst();
    $obj->debug($arr,$fl);
}

/**
 * @return string
 *
 * функция передачи управления заданному контроллеру через ajax
 */
function controller(){
    include_once '../constants.php';
    $data = json_decode($_POST['data'], true);
    $name = $_POST['controller'];
    include '../controllers/'.$name.'Controller.php';
    $controller = $name.'Controller';
    $obj = new $controller();
    $action = $_POST['func'];
    try{
        $res = $obj->$action($data);
        return json_encode($res);
    }catch (Exception $e){
        return json_encode('false');
    }
}